package pong;

import com.jogamp.opengl.GL;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.gl2.GLUT;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Random;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class jogo extends KeyAdapter implements GLEventListener {

    private float alfa;
    private float beta;
    private float xbolinha;
    private float ybolinha;
    private float ybolinhaant;
    private float ybot2;

    private float proxX;
    private float proxY;
    private float coeficienteAngularDaReta;
    private float coeficientelLinearDaReta = 0;
    private float fatorvelocidade = 1;
    private float direcaox = 0;
    private float direcaoy = 0;
    private float tamanho = 30f;
    private int pontoeu = 0;
    private int pontobot = 0;
    private float velocidade = 0;
    private Random gerador;
    private JLabel bot, eu;
    private JButton sobre;
    private boolean ponto = false;
    private JFrame frame;

    private float sinal = 1;

    public jogo() {
        GLProfile profile = GLProfile.get(GLProfile.GL2);
        GLCapabilities caps = new GLCapabilities(profile);
        caps.setDoubleBuffered(true);
        caps.setHardwareAccelerated(true);

//        jogo r = new jogo();
        GLCanvas canvas = new GLCanvas(caps);
        canvas.addGLEventListener(this);

        frame = new JFrame("JOGO - PONG");
        frame.addKeyListener(this);
        JPanel p = new JPanel();
        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
        JPanel j = new JPanel(new BorderLayout());

        bot = new JLabel("0");
        bot.setBorder(BorderFactory.createEmptyBorder(0, 50, 0, 0));
        bot.setFont(new java.awt.Font("Tahoma", 0, 36));
        eu = new JLabel("0");
        eu.setFont(new java.awt.Font("Tahoma", 0, 36));
        eu.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 50));
        j.add(bot, BorderLayout.WEST);
        j.add(eu, BorderLayout.EAST);
        canvas.setSize(900, 500);
        p.add(j);
        p.add(canvas);
        frame.add(p);

        frame.setSize(900, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        FPSAnimator animator = new FPSAnimator(canvas, 60);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        animator.start();
        alfa = 0;
        beta = 0;
        velocidade = 5f;

        pontoeu = 0;
        pontobot = 0;

        xbolinha = 1;
        ybolinha = 1;
        coeficienteAngularDaReta = 45;
        proxX = 0;
        proxY = 0;

        gerador = new Random();
    }

    public void attBolinha2() {
        float y2 = 0;

        if (direcaox == 0) {
            xbolinha = xbolinha + fatorvelocidade;
        } else {
            xbolinha = xbolinha - fatorvelocidade;
        }

        if (xbolinha == 0) {
            if (direcaox == 0) {
                xbolinha = xbolinha + 1;

            } else {
                xbolinha = xbolinha - 1;
            }
        }

        if (xbolinha > 200 || xbolinha < -200) {
            xbolinha = 1;
        } else {
            if (xbolinha + 10 == 200) {
                direcaox = 1;
            }
            if (xbolinha == -200) {
                direcaox = 0;
            }

        }
        if (direcaoy == 0) {
            ybolinha = ybolinha + fatorvelocidade;
        } else {
            ybolinha = ybolinha - fatorvelocidade;
        }

        if (ybolinha == 0) {
            if (direcaoy == 0) {
                ybolinha = ybolinha + 1;

            } else {
                ybolinha = ybolinha - 1;
            }
        }

        if (ybolinha > 100 || ybolinha < -100) {

        }
        if (ybolinha + 10 >= 100) {
            direcaoy = 1;
        }
        if (ybolinha <= -100) {
            direcaoy = 0;
        }

        if (xbolinha >= 160 && (ybolinha <= tamanho + alfa && ybolinha >= -tamanho + alfa)) {
            direcaox = 1;
        }
        if (xbolinha < 170 && ponto == true && xbolinha > -170) {
            ponto = false;
        }
        if (xbolinha >= 175 && ponto == false) {
            pontobot++;
            bot.setText("" + pontobot);
            ponto = true;
            fatorvelocidade = fatorvelocidade + 0.15f;
            xbolinha = ybolinha = 1;
        }

        if (xbolinha <= -170 && (ybolinha <= tamanho + beta && ybolinha >= -tamanho + beta)) {
            direcaox = 0;
        }
        if (xbolinha <= -175 && ponto == false) {
            pontoeu++;
            eu.setText("" + pontoeu);
            ponto = true;
            fatorvelocidade = fatorvelocidade + 0.15f;
            xbolinha = ybolinha = 1;
        }

    }

    public void display(GLAutoDrawable drawable) {

        GL2 gl = drawable.getGL().getGL2();
        GLUT glut = new GLUT();
        attBolinha2();
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);

        //Aqui carregamos a matriz responsável pelas transformações (vamos falar dela na próxima aula)
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();

        gl.glColor3f(1f, 1f, 1f);
        //PLAYER
        gl.glBegin(GL2.GL_POLYGON);
        gl.glVertex2f(170f, tamanho + alfa);
        gl.glVertex2f(170f, -tamanho + alfa);
        gl.glVertex2f(180f, -tamanho + alfa);
        gl.glVertex2f(180f, tamanho + alfa);
        gl.glEnd();

        gl.glColor3f(1f, 1f, 1f);

        //PLAYER 2 - BOT
        gl.glBegin(GL2.GL_POLYGON);
//        beta = (alfa + 2 * ybolinha) / 2;
        coeficienteAngularDaReta = ybolinha - ybolinhaant;
        if (xbolinha < 0) {
            if (direcaox == 1) {
                if (coeficienteAngularDaReta > 0 && ybolinha > tamanho + beta) {
                    beta = beta + 1 + gerador.nextInt(2);
                }
                if (coeficienteAngularDaReta < 0 && ybolinha < -tamanho + beta) {
                    beta = beta - 1 - gerador.nextInt(2);
                }
            }
        }
        beta = 3f + beta < (tamanho * 2) ? beta : (tamanho * 2) - 3;
        beta = -3f + beta > -(tamanho * 2) ? beta : -((tamanho * 2) - 3);
        gl.glVertex2f(-170f, tamanho + beta);
        gl.glVertex2f(-170f, -tamanho + beta);
        gl.glVertex2f(-180f, -tamanho + beta);
        gl.glVertex2f(-180f, tamanho + beta);
        gl.glEnd();

        //BOLINHA
        gl.glBegin(GL2.GL_POLYGON);
        gl.glVertex2f(xbolinha, ybolinha);
        gl.glVertex2f(xbolinha, ybolinha + 10);
        gl.glVertex2f(xbolinha + 10, ybolinha + 10);
        gl.glVertex2f(xbolinha + 10, ybolinha);
        gl.glEnd();

        ybolinhaant = ybolinha;
        gl.glFlush();

    }

    @Override
    public void keyPressed(KeyEvent e) {

        switch (e.getKeyCode()) {

            case KeyEvent.VK_UP:
                if (3f + alfa < tamanho * 2) {
                    alfa = alfa + velocidade;
                }

                break;
            case KeyEvent.VK_DOWN:
                if (-3f + alfa > -(tamanho * 2)) {
                    alfa = alfa - velocidade;
                }

                break;
            case KeyEvent.VK_W:
                if (3f + alfa < tamanho * 2) {
                    alfa = alfa + velocidade;
                }

                break;
            case KeyEvent.VK_S:
                if (-3f + alfa > -(tamanho * 2)) {
                    alfa = alfa - velocidade;
                }

                break;
            case KeyEvent.VK_ESCAPE:
                frame.dispose();
                new home().setVisible(true);
                break;

        }
    }


    @Override
    public void init(GLAutoDrawable drawable) {

        GL2 gl = drawable.getGL().getGL2();
        gl.glClearColor(0, 0, 0, 0);
        gl.glEnable(GL2.GL_DEPTH_TEST);

        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrtho(-200f, 200f, -100f, 100f, -100f, 100f);

    }

    @Override
    public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3, int arg4) {
        // TODO Auto-generated method stub

    }

    public static void main(String[] args) {
        new jogo();
    }

    @Override
    public void dispose(GLAutoDrawable glad) {
       
    }

}